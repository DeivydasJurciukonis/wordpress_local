<?php get_header(); ?>

<section class="site-section">
	<div class="container">
		<div class="row mb-4">
			<div class="col-md-6">
				<h2 class="mb-4"><?php _e( 'Category: ', 'balita' ); single_cat_title(); ?></h2>
			</div>
		</div>
		<div class="row blog-entries">
			<div class="col-md-12 col-lg-8 main-content">
				<div class="row mb-5 mt-5">
					<div class="col-md-12">
						<?php get_template_part('loop_category'); ?>
					</div>
				</div>

				<?php get_template_part('pagination'); ?>
			</div>
			<!-- END main-content -->

			<?php get_sidebar(); ?>
			<!-- END sidebar -->
		</div>
	</div>
</section>

<?php get_footer(); ?>
