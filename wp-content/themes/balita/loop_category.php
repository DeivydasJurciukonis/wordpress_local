<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<div class="post-entry-horzontal">
		<a href="<?php the_permalink(); ?>">
			<div class="image element-animate" data-animate-effect="fadeIn"
				<?php if ( has_post_thumbnail()) : ?>
					style="background-image: url(<?php the_post_thumbnail_url('cover'); ?>);"
				<?php endif; ?>
			></div>
			<span class="text">
				<div class="post-meta">
					<span class="category">Travel</span>
					<span class="mr-2"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?> </span> &bullet;
					<span class="ml-2"><span class="fa fa-comments"></span> 3</span>

					<?php //if (comments_open( get_the_ID() ) ) comments_popup_link( __( '0', 'balita' ), __( '1', 'balita' ), __( '%', 'balita' )); ?>
				</div>
				<h2><?php the_title(); ?></h2>
			</span>
		</a>
	</div>
<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
