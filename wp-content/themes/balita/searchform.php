<div class="sidebar-box search-form-wrap">
	<form class="search-form" method="get" action="<?php echo home_url(); ?>" role="search">
		<div class="form-group">
			<span class="icon fa fa-search"></span>
			<input class="form-control" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
		</div>
	</form>
</div>
